# GA-Config-Decoder

A project for decoding the configuration string send by every google analytics configuration. This could help validating the privacy claims of websites using google analytics. 

Website of the Config Decoder: [https://rufposten.de/blog/the-google-analytics-config-decoder/ ](https://rufposten.de/blog/the-google-analytics-config-decoder/)

## Help needed!

### Comparing different setups
You can help by analysing different configurations with the tool. 

 1. Copy "_u" parameter to the first input field
 2. Change one setting in your GA dashboard. 
 3. Reload your page and copy the different _u parameter to the second field. 
 4. Add description to the switch in the git. 

### Analysing the code of analytics.js
 
The analytics.js is also a good starting point to understand the meaning of the single switches:  
[https://stackoverflow.com/questions/26849042/u-parameter-in-universal-google-analytics-collect-hits](https://stackoverflow.com/questions/26849042/u-parameter-in-universal-google-analytics-collect-hits)